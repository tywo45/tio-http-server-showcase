package org.tio.http.server.showcase.model;

import org.tio.http.server.annotation.TioDoc;

@TioDoc(doc = "Job信息")
public class Job {
	private int		id;
	@TioDoc(doc = "Job名字")
	private String	name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Job() {
	}
}
