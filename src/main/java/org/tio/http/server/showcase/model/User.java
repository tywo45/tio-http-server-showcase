package org.tio.http.server.showcase.model;

import java.io.Serializable;

import org.tio.http.server.annotation.TioDoc;

@TioDoc(doc = "用户信息")
public class User extends UserParent implements Serializable {

	private static final long serialVersionUID = 7038736722168521022L;

}
