package org.tio.http.server.showcase.model;

import java.io.Serializable;

import org.tio.http.server.annotation.TioDoc;

public class UserParent implements Serializable {

	private static final long	serialVersionUID	= 7038736722168521022L;
	@TioDoc(doc = "id")
	private Integer				id;
	private Short				shortid;
	private Long				longid;
	private Byte				byteid;
	private Float				floatid;
	private Double				doubleid;
	@TioDoc(doc = "登录名999")
	private String				loginname;
	@TioDoc(doc = "昵称")
	private String				nick;
	private String				ip;
	@TioDoc(doc = "用户名")
	private String				useName;

	public Long getLongid() {
		return longid;
	}

	public void setLongid(Long longid) {
		this.longid = longid;
	}

	public Byte getByteid() {
		return byteid;
	}

	public void setByteid(Byte byteid) {
		this.byteid = byteid;
	}

	public Float getFloatid() {
		return floatid;
	}

	public void setFloatid(Float floatid) {
		this.floatid = floatid;
	}

	public Double getDoubleid() {
		return doubleid;
	}

	public void setDoubleid(Double doubleid) {
		this.doubleid = doubleid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginname() {
		return loginname;
	}

	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Short getShortid() {
		return shortid;
	}

	public void setShortid(Short shortid) {
		this.shortid = shortid;
	}

	public String getUseName() {
		return useName;
	}

	public void setUseName(String useName) {
		this.useName = useName;
	}

}
